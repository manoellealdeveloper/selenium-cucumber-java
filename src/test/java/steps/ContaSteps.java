package steps;

import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class ContaSteps {
	
	private WebDriver driver;
	
	@Dado("^que estou acessando a aplicação$")
	public void queEstouAcessandoAAplicação() {
	    System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.get("https://seubarriga.wcaquino.me/login");
	}

	@Quando("^informo o usuário \"([^\"]*)\"$")
	public void informoOUsuário(String arg1) throws Throwable {
	    driver.findElement(By.id("email")).sendKeys(arg1);
	}

	@Quando("^a senha \"([^\"]*)\"$")
	public void aSenha(String arg1) throws Throwable {
	    driver.findElement(By.id("senha")).sendKeys(arg1);
	}

	@Quando("^seleciono entrar$")
	public void selecionoEntrar() throws Throwable {
	    driver.findElement(By.xpath("//button")).click();
	}

	@Então("^visualizo a página inicial$")
	public void visualizoAPáginaInicial() throws Throwable {
	    String mensagem = driver.findElement(By.xpath("//div[@role=\"alert\"]")).getText();
	    Assert.assertEquals("Bem vindo, Ruan André Aragão!", mensagem);
	}

	@Quando("^seleciono Contas$")
	public void selecionoContas() throws Throwable {
	    driver.findElement(By.xpath("//a[@class=\"dropdown-toggle\"]")).click();
	}

	@Quando("^seleciono Adicionar$")
	public void selecionoAdicionar() throws Throwable {
	    driver.findElement(By.linkText("Adicionar")).click();
	}

	@Quando("^informo a conta \"(.*)\"$")
	public void informoAConta(String arg1) throws Throwable {
		if(arg1.contains("Glenn Legros")) {
			driver.findElement(By.id("nome")).sendKeys(arg1);
		} else if (arg1.equals("")) {
			driver.findElement(By.id("nome")).sendKeys(arg1);
		} else {	
			Date timeStamp = new Date();
			driver.findElement(By.id("nome")).sendKeys(arg1 + timeStamp.getTime());
		}
		
	}

	@Quando("^seleciono Salvar$")
	public void selecionoSalvar() throws Throwable {
	    driver.findElement(By.xpath("//button")).click();
	}
	
	@Então("^recebo a mensagem \"([^\"]*)\"$")
	public void recebo_a_mensagem(String arg1) throws Throwable {
		String mensagem = driver.findElement(By.xpath("//div[@role='alert']")).getText();
	    Assert.assertEquals(arg1, mensagem);
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}

}
